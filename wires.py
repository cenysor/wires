#! python3

# Implementation of Joscha Bach's "Wireworld" game
# http://bach.ai/a-puzzle-game-that-makes-us-build-a-complete-computer

import pygame
import sys

def redraw(matrix):
    #paint the grid
    for x in range(grid):
        for y in range(grid):
            win.blit(matrix[x][y], (x*50, y*50))
    
    pygame.display.update()

pygame.init()

#define height and width of the grid
grid = 15

win = pygame.display.set_mode((grid * 50, grid * 50))

pygame.display.set_caption('Wires')

#load images
blank = pygame.image.load('blank.png')
horizontal = pygame.image.load('horizontal.png')
vertical = pygame.image.load('vertical.png')
cross = pygame.image.load('cross.png')
rpower = pygame.image.load('rpower.png')
dpower = pygame.image.load('dpower.png')
lpower = pygame.image.load('lpower.png')
upower = pygame.image.load('upower.png')
phorizontal = pygame.image.load('phorizontal.png')
pvertical = pygame.image.load('pvertical.png')
pcross = pygame.image.load('pcross.png')
rlamp = pygame.image.load('rlamp.png')
dlamp = pygame.image.load('dlamp.png')
llamp = pygame.image.load('llamp.png')
ulamp = pygame.image.load('ulamp.png')
prlamp = pygame.image.load('prlamp.png')
pdlamp = pygame.image.load('pdlamp.png')
pllamp = pygame.image.load('pllamp.png')
pulamp = pygame.image.load('pulamp.png')
roswitch = pygame.image.load('roswitch.png')
doswitch = pygame.image.load('doswitch.png')
loswitch = pygame.image.load('loswitch.png')
uoswitch = pygame.image.load('uoswitch.png')
rcswitch = pygame.image.load('rcswitch.png')
dcswitch = pygame.image.load('dcswitch.png')
lcswitch = pygame.image.load('lcswitch.png')
ucswitch = pygame.image.load('ucswitch.png')
prcswitch = pygame.image.load('prcswitch.png')
pdcswitch = pygame.image.load('pdcswitch.png')
plcswitch = pygame.image.load('plcswitch.png')
pucswitch = pygame.image.load('pucswitch.png')
bridge = pygame.image.load('bridge.png')
phbridge = pygame.image.load('phbridge.png')
pvbridge = pygame.image.load('pvbridge.png')
pdbridge = pygame.image.load('pdbridge.png')

#fill the grid with blank
matrix = []
line = []

for x in range(grid):
    line.append(blank)
for y in range(grid):
    matrix.append(list(line))

print('''
Welcome to Wires, an implementation of Joscha Bach\'s "Wireworld" game!

http://bach.ai/a-puzzle-game-that-makes-us-build-a-complete-computer

controls:
[left mouse click] create wire ore toggle orientation of object
[p] create power source
[s] create switch
[l] create lamp
[d] delete object''')

#main loop
while True:
    pygame.time.delay(200)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        #switch to different image at mouse position
        if event.type == pygame.MOUSEBUTTONDOWN:
            mx, my = pygame.mouse.get_pos()
            if matrix[mx // 50][my // 50] == blank:
                matrix[mx // 50][my // 50] = horizontal
            elif matrix[mx // 50][my // 50] == horizontal:
                matrix[mx // 50][my // 50] = vertical
            elif matrix[mx // 50][my // 50] == vertical:
                matrix[mx // 50][my // 50] = cross
            elif matrix[mx // 50][my // 50] == cross:
                matrix[mx // 50][my // 50] = bridge
            elif matrix[mx // 50][my // 50] == bridge:
                matrix[mx // 50][my // 50] = horizontal
            elif matrix[mx // 50][my // 50] == rpower:
                matrix[mx // 50][my // 50] = dpower
            elif matrix[mx // 50][my // 50] == dpower:
                matrix[mx // 50][my // 50] = lpower
            elif matrix[mx // 50][my // 50] == lpower:
                matrix[mx // 50][my // 50] = upower
            elif matrix[mx // 50][my // 50] == upower:
                matrix[mx // 50][my // 50] = rpower
            elif matrix[mx // 50][my // 50] == phorizontal:
                matrix[mx // 50][my // 50] = vertical
            elif matrix[mx // 50][my // 50] == pvertical:
                matrix[mx // 50][my // 50] = cross
            elif matrix[mx // 50][my // 50] == pcross:
                matrix[mx // 50][my // 50] = bridge
            elif matrix[mx // 50][my // 50] == phbridge:
                matrix[mx // 50][my // 50] = horizontal
            elif matrix[mx // 50][my // 50] == pvbridge:
                matrix[mx // 50][my // 50] = horizontal
            elif matrix[mx // 50][my // 50] == pdbridge:
                matrix[mx // 50][my // 50] = horizontal
            elif matrix[mx // 50][my // 50] == rlamp:
                matrix[mx // 50][my // 50] = dlamp
            elif matrix[mx // 50][my // 50] == dlamp:
                matrix[mx // 50][my // 50] = llamp
            elif matrix[mx // 50][my // 50] == llamp:
                matrix[mx // 50][my // 50] = ulamp
            elif matrix[mx // 50][my // 50] == ulamp:
                matrix[mx // 50][my // 50] = rlamp
            elif matrix[mx // 50][my // 50] == prlamp:
                matrix[mx // 50][my // 50] = dlamp
            elif matrix[mx // 50][my // 50] == pdlamp:
                matrix[mx // 50][my // 50] = llamp
            elif matrix[mx // 50][my // 50] == pllamp:
                matrix[mx // 50][my // 50] = ulamp
            elif matrix[mx // 50][my // 50] == pulamp:
                matrix[mx // 50][my // 50] = rlamp
            elif matrix[mx // 50][my // 50] == rcswitch:
                matrix[mx // 50][my // 50] = dcswitch
            elif matrix[mx // 50][my // 50] == dcswitch:
                matrix[mx // 50][my // 50] = lcswitch
            elif matrix[mx // 50][my // 50] == lcswitch:
                matrix[mx // 50][my // 50] = ucswitch
            elif matrix[mx // 50][my // 50] == ucswitch:
                matrix[mx // 50][my // 50] = rcswitch
            elif matrix[mx // 50][my // 50] == prcswitch:
                matrix[mx // 50][my // 50] = dcswitch
            elif matrix[mx // 50][my // 50] == pdcswitch:
                matrix[mx // 50][my // 50] = lcswitch
            elif matrix[mx // 50][my // 50] == plcswitch:
                matrix[mx // 50][my // 50] = ucswitch
            elif matrix[mx // 50][my // 50] == pucswitch:
                matrix[mx // 50][my // 50] = rcswitch
            elif matrix[mx // 50][my // 50] == roswitch:
                matrix[mx // 50][my // 50] = dcswitch
            elif matrix[mx // 50][my // 50] == doswitch:
                matrix[mx // 50][my // 50] = lcswitch
            elif matrix[mx // 50][my // 50] == loswitch:
                matrix[mx // 50][my // 50] = ucswitch
            elif matrix[mx // 50][my // 50] == uoswitch:
                matrix[mx // 50][my // 50] = rcswitch

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_p:
                mx, my = pygame.mouse.get_pos()
                matrix[mx // 50][my // 50] = rpower
            if event.key == pygame.K_l:
                mx, my = pygame.mouse.get_pos()
                matrix[mx // 50][my // 50] = rlamp
            if event.key == pygame.K_s:
                mx, my = pygame.mouse.get_pos()
                matrix[mx // 50][my // 50] = rcswitch
            if event.key == pygame.K_d:
                mx, my = pygame.mouse.get_pos()
                matrix[mx // 50][my // 50] = blank
                
    #reset the circuit
    for x in range(grid):
        for y in range(grid):
            if matrix[x][y] == phorizontal:
                matrix[x][y] = horizontal
            elif matrix[x][y] == pvertical:
                matrix[x][y] = vertical
            elif matrix[x][y] == pcross:
                matrix[x][y] = cross
            elif matrix[x][y] == prlamp:
                matrix[x][y] = rlamp
            elif matrix[x][y] == pdlamp:
                matrix[x][y] = dlamp
            elif matrix[x][y] == pllamp:
                matrix[x][y] = llamp
            elif matrix[x][y] == pulamp:
                matrix[x][y] = ulamp
            elif matrix[x][y] == prcswitch:
                matrix[x][y] = rcswitch
            elif matrix[x][y] == pdcswitch:
                matrix[x][y] = dcswitch
            elif matrix[x][y] == plcswitch:
                matrix[x][y] = lcswitch
            elif matrix[x][y] == pucswitch:
                matrix[x][y] = ucswitch
            elif matrix[x][y] == phbridge:
                matrix[x][y] = bridge
            elif matrix[x][y] == pvbridge:
                matrix[x][y] = bridge
            elif matrix[x][y] == pdbridge:
                matrix[x][y] = bridge

    #power the circuit
    for i in range(grid):
        for x in range(grid):
            for y in range(grid):
                if matrix[x][y] == rpower:
                    if matrix[x + 1][y] == horizontal:
                        matrix[x + 1][y] = phorizontal
                    elif matrix[x + 1][y] == dcswitch:
                        matrix[x + 1][y] = pdcswitch
                    elif matrix[x + 1][y] == ucswitch:
                        matrix[x + 1][y] = pucswitch
                    elif matrix[x + 1][y] == cross:
                        matrix[x + 1][y] = pcross
                    elif matrix[x + 1][y] == llamp:
                        matrix[x + 1][y] = pllamp
                    elif matrix[x + 1][y] == bridge:
                        matrix[x + 1][y] = phbridge
                    elif matrix[x + 1][y] == pvbridge:
                        matrix[x + 1][y] = pdbridge
                elif matrix[x][y] == lpower:
                    if matrix[x - 1][y] == horizontal:
                        matrix[x - 1][y] = phorizontal
                    elif matrix[x - 1][y] == dcswitch:
                        matrix[x - 1][y] = pdcswitch
                    elif matrix[x - 1][y] == ucswitch:
                        matrix[x - 1][y] = pucswitch
                    elif matrix[x - 1][y] == cross:
                        matrix[x - 1][y] = pcross
                    elif matrix[x - 1][y] == rlamp:
                        matrix[x - 1][y] = prlamp
                    elif matrix[x - 1][y] == bridge:
                        matrix[x - 1][y] = phbridge
                    elif matrix[x - 1][y] == pvbridge:
                        matrix[x - 1][y] = pdbridge
                elif matrix[x][y] == dpower:
                    if matrix[x][y + 1] == vertical:
                        matrix[x][y + 1] = pvertical
                    elif matrix[x][y + 1] == rcswitch:
                        matrix[x][y + 1] = prcswitch
                    elif matrix[x][y + 1] == lcswitch:
                        matrix[x][y + 1] = plcswitch
                    elif matrix[x][y + 1] == cross:
                        matrix[x][y + 1] = pcross
                    elif matrix[x][y + 1] == ulamp:
                        matrix[x][y + 1] = pulamp
                    elif matrix[x][y + 1] == bridge:
                        matrix[x][y + 1] = pvbridge
                    elif matrix[x][y + 1] == phbridge:
                        matrix[x][y + 1] = pdbridge
                elif matrix[x][y] == upower:
                    if matrix[x][y - 1] == vertical:
                        matrix[x][y - 1] = pvertical
                    elif matrix[x][y - 1] == rcswitch:
                        matrix[x][y - 1] = prcswitch
                    elif matrix[x][y - 1] == lcswitch:
                        matrix[x][y - 1] = plcswitch
                    elif matrix[x][y - 1] == cross:
                        matrix[x][y - 1] = pcross
                    elif matrix[x][y - 1] == dlamp:
                        matrix[x][y - 1] = pdlamp
                    elif matrix[x][y - 1] == bridge:
                        matrix[x][y - 1] = pvbridge
                    elif matrix[x][y - 1] == phbridge:
                        matrix[x][y - 1] = pdbridge
                elif matrix[x][y] == phorizontal or matrix[x][y] == pdcswitch or matrix[x][y] == pucswitch or matrix[x][y] == phbridge:
                    if matrix[x + 1][y] == horizontal:
                        matrix[x + 1][y] = phorizontal
                    elif matrix[x + 1][y] == dcswitch:
                        matrix[x + 1][y] = pdcswitch
                    elif matrix[x + 1][y] == ucswitch:
                        matrix[x + 1][y] = pucswitch
                    elif matrix[x + 1][y] == cross:
                        matrix[x + 1][y] = pcross
                    elif matrix[x + 1][y] == llamp:
                        matrix[x + 1][y] = pllamp
                    elif matrix[x + 1][y] == bridge:
                        matrix[x + 1][y] = phbridge
                    elif matrix[x + 1][y] == pvbridge:
                        matrix[x + 1][y] = pdbridge
                    elif matrix[x - 1][y] == horizontal:
                        matrix[x - 1][y] = phorizontal
                    elif matrix[x - 1][y] == dcswitch:
                        matrix[x - 1][y] = pdcswitch
                    elif matrix[x - 1][y] == ucswitch:
                        matrix[x - 1][y] = pucswitch
                    elif matrix[x - 1][y] == cross:
                        matrix[x - 1][y] = pcross
                    elif matrix[x - 1][y] == rlamp:
                        matrix[x - 1][y] = prlamp
                    elif matrix[x - 1][y] == bridge:
                        matrix[x - 1][y] = phbridge
                    elif matrix[x - 1][y] == pvbridge:
                        matrix[x - 1][y] = pdbridge
                elif matrix[x][y] == pvertical or matrix[x][y] == prcswitch or matrix[x][y] == plcswitch or matrix[x][y] == pvbridge:
                    if matrix[x][y + 1] == vertical:
                        matrix[x][y + 1] = pvertical
                    elif matrix[x][y + 1] == rcswitch:
                        matrix[x][y + 1] = prcswitch
                    elif matrix[x][y + 1] == lcswitch:
                        matrix[x][y + 1] = plcswitch
                    elif matrix[x][y + 1] == cross:
                        matrix[x][y + 1] = pcross
                    elif matrix[x][y + 1] == ulamp:
                        matrix[x][y + 1] = pulamp
                    elif matrix[x][y + 1] == bridge:
                        matrix[x][y + 1] = pvbridge
                    elif matrix[x][y + 1] == phbridge:
                        matrix[x][y + 1] = pdbridge
                    elif matrix[x][y - 1] == vertical:
                        matrix[x][y - 1] = pvertical
                    elif matrix[x][y - 1] == rcswitch:
                        matrix[x][y - 1] = prcswitch
                    elif matrix[x][y - 1] == lcswitch:
                        matrix[x][y - 1] = plcswitch
                    elif matrix[x][y - 1] == cross:
                        matrix[x][y - 1] = pcross
                    elif matrix[x][y - 1] == dlamp:
                        matrix[x][y - 1] = pdlamp
                    elif matrix[x][y - 1] == bridge:
                        matrix[x][y - 1] = pvbridge
                    elif matrix[x][y - 1] == phbridge:
                        matrix[x][y - 1] = pdbridge
                elif matrix[x][y] == pcross or matrix[x][y] == pdbridge:
                    if matrix[x + 1][y] == horizontal:
                        matrix[x + 1][y] = phorizontal
                    elif matrix[x + 1][y] == dcswitch:
                        matrix[x + 1][y] = pdcswitch
                    elif matrix[x + 1][y] == ucswitch:
                        matrix[x + 1][y] = pucswitch
                    elif matrix[x + 1][y] == cross:
                        matrix[x + 1][y] = pcross
                    elif matrix[x + 1][y] == llamp:
                        matrix[x + 1][y] = pllamp
                    elif matrix[x + 1][y] == bridge:
                        matrix[x + 1][y] = phbridge
                    elif matrix[x + 1][y] == pvbridge:
                        matrix[x + 1][y] = pdbridge
                    elif matrix[x - 1][y] == horizontal:
                        matrix[x - 1][y] = phorizontal
                    elif matrix[x - 1][y] == dcswitch:
                        matrix[x - 1][y] = pdcswitch
                    elif matrix[x - 1][y] == ucswitch:
                        matrix[x - 1][y] = pucswitch
                    elif matrix[x - 1][y] == cross:
                        matrix[x - 1][y] = pcross
                    elif matrix[x - 1][y] == rlamp:
                        matrix[x - 1][y] = prlamp
                    elif matrix[x - 1][y] == bridge:
                        matrix[x - 1][y] = phbridge
                    elif matrix[x - 1][y] == pvbridge:
                        matrix[x - 1][y] = pdbridge
                    elif matrix[x][y + 1] == vertical:
                        matrix[x][y + 1] = pvertical
                    elif matrix[x][y + 1] == rcswitch:
                        matrix[x][y + 1] = prcswitch
                    elif matrix[x][y + 1] == lcswitch:
                        matrix[x][y + 1] = plcswitch
                    elif matrix[x][y + 1] == cross:
                        matrix[x][y + 1] = pcross
                    elif matrix[x][y + 1] == ulamp:
                        matrix[x][y + 1] = pulamp
                    elif matrix[x][y + 1] == bridge:
                        matrix[x][y + 1] = pvbridge
                    elif matrix[x][y + 1] == phbridge:
                        matrix[x][y + 1] = pdbridge
                    elif matrix[x][y - 1] == vertical:
                        matrix[x][y - 1] = pvertical
                    elif matrix[x][y - 1] == rcswitch:
                        matrix[x][y - 1] = prcswitch
                    elif matrix[x][y - 1] == lcswitch:
                        matrix[x][y - 1] = plcswitch
                    elif matrix[x][y - 1] == cross:
                        matrix[x][y - 1] = pcross
                    elif matrix[x][y - 1] == dlamp:
                        matrix[x][y - 1] = pdlamp
                    elif matrix[x][y - 1] == bridge:
                        matrix[x][y - 1] = pvbridge
                    elif matrix[x][y - 1] == phbridge:
                        matrix[x][y - 1] = pdbridge

    redraw(matrix)

    #all switches off
    for x in range(grid):
        for y in range(grid):
            if matrix[x][y] == roswitch:
                matrix[x][y] = rcswitch
            elif matrix[x][y] == doswitch:
                matrix[x][y] = dcswitch
            elif matrix[x][y] == loswitch:
                matrix[x][y] = lcswitch
            elif matrix[x][y] == uoswitch:
                matrix[x][y] = ucswitch

    #switch switches on
    for x in range(grid):
        for y in range(grid):
            if matrix[x][y] == rcswitch or matrix[x][y] == prcswitch:
                if matrix[x + 1][y] == lpower:
                    matrix[x][y] = roswitch
                elif matrix[x + 1][y] == phorizontal:
                    matrix[x][y] = roswitch
                elif matrix[x + 1][y] == phbridge:
                    matrix[x][y] = roswitch
                elif matrix[x + 1][y] == pcross:
                    matrix[x][y] = roswitch
                elif matrix[x + 1][y] == pdbridge:
                    matrix[x][y] = roswitch
                elif matrix[x + 1][y] == pdcswitch:
                    matrix[x][y] = roswitch
                elif matrix[x + 1][y] == pucswitch:
                    matrix[x][y] = roswitch
            elif matrix[x][y] == lcswitch or matrix[x][y] == plcswitch:
                if matrix[x - 1][y] == rpower:
                    matrix[x][y] = loswitch
                elif matrix[x - 1][y] == phorizontal:
                    matrix[x][y] = loswitch
                elif matrix[x - 1][y] == phbridge:
                    matrix[x][y] = loswitch
                elif matrix[x - 1][y] == pcross:
                    matrix[x][y] = loswitch
                elif matrix[x - 1][y] == pdbridge:
                    matrix[x][y] = loswitch
                elif matrix[x - 1][y] == pdcswitch:
                    matrix[x][y] = loswitch
                elif matrix[x - 1][y] == pucswitch:
                    matrix[x][y] = loswitch
            elif matrix[x][y] == ucswitch or matrix[x][y] == pucswitch:
                if matrix[x][y - 1] == dpower:
                    matrix[x][y] = uoswitch
                elif matrix[x][y - 1] == pvertical:
                    matrix[x][y] = uoswitch
                elif matrix[x][y - 1] == pvbridge:
                    matrix[x][y] = uoswitch
                elif matrix[x][y - 1] == pcross:
                    matrix[x][y] = uoswitch
                elif matrix[x][y - 1] == pdbridge:
                    matrix[x][y] = uoswitch
                elif matrix[x][y - 1] == prcswitch:
                    matrix[x][y] = uoswitch
                elif matrix[x][y - 1] == plcswitch:
                    matrix[x][y] = uoswitch
            elif matrix[x][y] == dcswitch or matrix[x][y] == pdcswitch:
                if matrix[x][y + 1] == upower:
                    matrix[x][y] = doswitch
                elif matrix[x][y + 1] == pvertical:
                    matrix[x][y] = doswitch
                elif matrix[x][y + 1] == pvbridge:
                    matrix[x][y] = doswitch
                elif matrix[x][y + 1] == pcross:
                    matrix[x][y] = doswitch
                elif matrix[x][y + 1] == pdbridge:
                    matrix[x][y] = doswitch
                elif matrix[x][y + 1] == prcswitch:
                    matrix[x][y] = doswitch
                elif matrix[x][y + 1] == plcswitch:
                    matrix[x][y] = doswitch
